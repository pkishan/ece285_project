import sys
import torch
import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt


def main():
    path = sys.argv[1]
    results = torch.load(path)
    
    train_acc = torch.FloatTensor(results['tracker']['train_acc'])
    train_acc = train_acc.mean(dim=1).numpy()

    val_acc = torch.FloatTensor(results['tracker']['val_acc'])
    val_acc = val_acc.mean(dim=1).numpy()

    plt.figure()
   # plt.plot(val_acc)
   # plt.savefig('val_acc.png')
    plt.plot(train_acc,label='Train Accuracy')
    plt.plot(val_acc, label = 'Validation Accuracy')
    plt.title('Accuracy vs No. of Epochs')
    plt.legend(loc = 'lower right')
    plt.xlabel('No. of Epochs')
    plt.ylabel('Accuracy')




    plt.grid(True)
    plt.savefig('train&val_acc.png')
    

if __name__ == '__main__':
    main()
